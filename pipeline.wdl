version 1.1

import "tasks/umi_collapse.wdl" as umi_collapse
import "tasks/bwa.wdl" as bwa
import "tasks/mutect2.wdl" as mutect2

workflow test_pipeline {
    meta {
        developer: "George Doyle"
        date: "22/11/2022"
        version: "1.0.0"
    }
    input {
        String sample_name
        File trimmed_fastq_R1
        File trimmed_fastq_R2
	    File master_bed
        File genome_fa
        File genome_fa_dict
        File genome_fa_index
        File genome_fa_bwa_index
    }

        # BWA-MEM
        call bwa.bwa_mem {
            input:
            sample_name = sample_name,
            trimmed_fastq_R1 = trimmed_fastq_R1,
            trimmed_fastq_R2 = trimmed_fastq_R2,
            bwa_index = genome_fa_bwa_index
        }


        # UMICOLLAPSE
        call umi_collapse.UMICollapse {
            input:
            sample_name = sample_name,
            precollapsed_bam = bwa_mem.precollapsed_bam,
            precollapsed_bam_index = bwa_mem.precollapsed_bam_index
	}

        # MUTECT2
        call mutect2.Mutect2 as m2{
            input:
                sample_name = sample_name,
                reference = genome_fa
                reference_index = genome_fa_index
                mappings_bam = UMICollapse.final_bam,
                mappings_bai = UMICollapse.final_bam_index,
                intervals = master_bed,
                dockerImage = "swglh/nbt-sentieon"
        }
}

version 1.1

task bwa_mem {
    input {
        String sample_name
        File? trimmed_fastq_R1
        File? trimmed_fastq_R2
        File bwa_index
    }

    command <<<
        export bwt_max_mem=16G
        mkdir genome &&
        tar -zxvf ~{bwa_index} -C genome && # unpack indexes
        genome_file=`ls genome/*.bwt` # get one of the index files
        genome_file="${genome_file%.bwt}" && \
        bwa mem \
        -M \
        -R $(echo "@RG\tID:~{sample_name}\tSM:~{sample_name}\tPL:ILLUMINA\tLB:~{sample_name}") \
        -t 36 \
        -K 10000000 \
        ${genome_file} \
        ~{trimmed_fastq_R1} ~{trimmed_fastq_R2} \
        | \
        sentieon util sort \
        -t 36 \
        -o ~{sample_name}.precollapsed.bam \
        --block_size 256M \
        --sam2bam \
        -i -
    >>>
    output {
        File? precollapsed_bam = "${sample_name}.precollapsed.bam"
        File? precollapsed_bam_index = "${sample_name}.precollapsed.bam.bai"
    }
    runtime {
        instanceType: "mem1_ssd1_v2_x36"
        docker: "swglh/nbt-sentieon"
        memory: "72 GB"
        cpu: 36
    }
}

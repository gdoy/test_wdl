version 1.1

task UMICollapse {
    input {
        File? precollapsed_bam
        File? precollapsed_bam_index
        String sample_name
        Int? min_disk_gb = 10
    }

    command <<<
    /umicollapse bam \
    --two-pass \
    -i ~{precollapsed_bam} \
    -o ~{sample_name}.final.bam &&
    samtools index ~{sample_name}.final.bam
    >>>
    output {
        File? final_bam = "${sample_name}.final.bam"
        File? final_bam_index = "${sample_name}.final.bam.bai"
    }
    runtime {
        instanceType: "mem1_ssd1_v2_x16"
        docker: "mattwherlock/umicollapse-samtools-reorg:latest"
        memory: "32 GB"
        cpu: 16
    }
}

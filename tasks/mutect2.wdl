version 1.1

task Mutect2 {
    input {
        String sample_name
        File? mappings_bam
        File? mappings_bai
        File reference
        File reference_index
        File intervals
        String dockerImage
    }

    command <<<
        touch ~{mappings_bai}
        touch ~{reference_index}
        sentieon driver \
        -i ~{mappings_bam} \
        -r ~{reference} \
        --interval ~{intervals} \
        --interval_padding 50 \
        --algo TNhaplotyper2 \
        --tumor_sample ~{sample_name} \
        --bam_output ~{sample_name}_m2.bam \
        ~{sample_name}_mutect2.vcf
    >>>

    output {
        File? vcf = "${sample_name}_mutect2.vcf"
        File? bam = "${sample_name}_m2.bam"
        File? bai = "${sample_name}_m2.bai"
        File? stats= "${sample_name}_mutect2.vcf.stats"
    }

    runtime {
        instanceType: "mem1_ssd1_v2_x36"
        docker: "swglh/nbt-sentieon"
        memory: "72 GB"
        cpu: 36
    }

}

task FilterCalls{
    input {
        String sample_name
        File reference
        File reference_index
        File intervals
        File? unfiltered_vcf
        File? stats
        String dockerImage
    }

    command <<<
        touch ~{reference_index}
        sentieon driver \
        -r ~{reference} \
        --algo TNfilter \
        --tumor_sample ~{sample_name} \
        --max_event_count 15 \
        -v ~{unfiltered_vcf} \
        ~{sample_name}_mutect2_filt.vcf
    >>>

    output {
        File? vcf = "${sample_name}_mutect2_filt.vcf"
    }

    runtime {
        instanceType: "mem1_ssd1_v2_x4"
        docker: "swglh/nbt-sentieon"
        memory: "8 GB"
        cpu: 4
    }
}

# Introduction

Simplified workflow designed for testing the Sentieon suite tools. Can be used to process single samples (paired-end). Uses BWA-MEM, UMICollapse, and Mutect2 tools to align reads, collapse UMI duplicates, and perform somatic variant calling.

## Prerequisites

- WDL for running the workflow.
- Docker for running tools in isolated environments.

## Installation

Clone this repository:

```
git clone https://github.com/yourusername/test_pipeline.git
cd test_pipeline
```

Ensure that the required tools are available in your environment. The tools are specified as Docker images in the WDL files.

## Usage

Edit the input parameters in the test_pipeline.wdl file to match your sample data and reference files.

Run the workflow using your preferred WDL execution engine. For example, using Cromwell:

### Input Parameters

- sample_name: A string representing the name of the sample being processed.
- trimmed_fastq_R1: R1 reads in FASTQ format.
- trimmed_fastq_R2: R2 reads in FASTQ format.
- master_bed: BED file with target regions.
- genome_fa: Reference genome in FASTA format.
- genome_fa_dict: Reference genome dictionary.
- genome_fa_index: Reference genome index.
- genome_fa_bwa_index: Gzipped BWA index for the reference genome.

### Output Files

- vcf: A VCF file containing somatic variant calls.
- bam: A BAM file containing the aligned and processed reads.
- bai: A BAM index file.
- stats: A file containing statistics about the called variants.

## Authors

George Doyle

## Licence

This project is licensed under the MIT License.